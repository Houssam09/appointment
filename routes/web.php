<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('cache', function() {
    Artisan::call('config:cache');
    Artisan::call('route:clear');
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('view:clear');
    return '<h1>Ok !</h1>';
});

Route::get('install', function() {
    Artisan::call('migrate');
    Artisan::call('db:seed');
    return '<h1>Ok !</h1>';
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'],function (){
    Route::get('/login', 'Auth\AdminsLoginController@showForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminsLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\AdminsLoginController@logout')->name('admin.logout');
    Route::group(['middleware' => 'auth:admin'] ,function (){
        Route::get('/', 'AdminsController@index')->name('admin.dashboard');
        Route::resource('clients', 'Admin\ClientsController');
        Route::post('appointment/getMonthsDays', 'Admin\AppointmentsController@getMonthsDays');
        Route::resource('appointment', 'Admin\AppointmentsController');
        //Route::resource('waiting', 'Admin\WaitingController');
        Route::get('settings', 'Admin\GeneralSettingsController@index')->name('admin.settings');
        Route::patch('settings', 'Admin\GeneralSettingsController@store');
        //ProfileController
        Route::post('setAsPresentRDV','CalendarsController@setAsPresentRDV')->name('present');
        Route::post('deleteRDV','CalendarsController@softDeleteRDV')->name('deleteRDV');
        Route::get('profile/edit-profile','ProfileController@index')->name('admin.profile.index');
        Route::post('profile/edit-profile', 'ProfileController@index')->name('admin.profile.index');
        Route::get('profile/change-password', 'ProfileController@password')->name('admin.profile.password');
        Route::post('profile/change-password', 'ProfileController@password')->name('admin.profile.password');
        // UploadController
        Route::post('upload', 'UploadController@index')->name('admin.upload');
    });

});
Route::resource('calendar', 'CalendarsController');
Route::group(['middleware' => 'auth'] , function() {
    //ProfileController
    Route::get('profile/edit-profile','ProfileController@index')->name('profile.index');
    Route::post('profile/edit-profile', 'ProfileController@index')->name('profile.index');
    Route::get('profile/change-password', 'ProfileController@password')->name('profile.password');
    Route::post('profile/change-password', 'ProfileController@password')->name('profile.password');
    // UploadController
    Route::post('upload', 'UploadController@index')->name('upload');
    //CalendarsController
   // Route::resource('calendar', 'CalendarsController');
   Route::post('proposerdv','CalendarsController@proposeRdv')->name('propose');
   Route::post('annulerRDV','CalendarsController@deleteRDV')->name('annulerRDV');
  // Route::get('proposerdv','CalendarsController@proposeRdv')->name('propose');
});
