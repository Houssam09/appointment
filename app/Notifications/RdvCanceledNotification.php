<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User; 
class RdvCanceledNotification extends Notification
{
    use Queueable;

    private $rdv; 
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($rdv)
    {
        $this->rdv = $rdv; 
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $rdv  = $this->rdv;
        $user = User::find($rdv->user_id); 
        return [
            "label"   =>  $rdv->label.' - '.$user->company,
            "date"    =>  $rdv->start,
        ];
    }
}
