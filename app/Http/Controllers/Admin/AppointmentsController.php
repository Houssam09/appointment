<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = 1;
        $appointments = Appointment::all();
        return view('admin.appointments.index', compact('appointments', 'num'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $appointment = new Appointment();
        return view('admin.appointments.create', compact('appointment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pre_data = $request;
        if($pre_data['type'] == 'fixed'){
            $data['type'] = 'fixed';

            if(isset($pre_data['fixed_label'])){
                $data['label'] = $pre_data['fixed_label'];
            }
            if(isset($pre_data['fixed_months'])){
                $data['months'] = json_encode($pre_data['fixed_months']);
            }
            if(isset($pre_data['fixed_days'])){
                $data['days'] = json_encode($pre_data['fixed_days']);
            }
            if(isset($pre_data['fixed_work_start'])){
                $data['time'] = json_encode($pre_data['fixed_work_start']. '=>'.$pre_data['fixed_work_end'] );
            }
            $data['all_day'] = $pre_data['all_day'];
            $data['custom_hvolume'] = $pre_data['custom_hvolume'];
        }else{
            $data['type'] = 'custom';
            if(isset($pre_data['custom_label'])){
                $data['label'] = $pre_data['custom_label'];
            }
            for ($i=1; $i <= $request['rows_num']; $i++){

                if(isset($pre_data['custom_months_'. $i])){
                    $data['months'][$i] = $pre_data['custom_months_'. $i];
                }
                if(isset($pre_data['custom_days_'. $i])){
                    $data['days'][$i] = $pre_data['custom_days_'. $i];
                }

                if(isset($pre_data['custom_work_start_'. $i])){
                    $data['time'][$i] = $pre_data['custom_work_start_'. $i]. '=>'.$pre_data['custom_work_end_'. $i];
                }

                if(isset($pre_data['custom_hvolume_'. $i])){
                    $data['custom_hvolume'][$i] = $pre_data['custom_hvolume_'. $i];
                }
            }

            $data['months'] = json_encode($data['months']);
            $data['days'] = json_encode($data['days']);
            $data['time'] = json_encode($data['time']);
            $data['custom_hvolume'] = json_encode($data['custom_hvolume']);
        }

        Appointment::create($data);
        return redirect('admin/appointment');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        return view('admin.appointments.edit', compact('appointment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {

        $pre_data = $request;
        $data['label'] = $pre_data['label'];
        if($pre_data['type'] == 'fixed'){
            $data['type'] = 'fixed';
            if(isset($pre_data['fixed_months'])){
                $data['months'] = json_encode($pre_data['fixed_months']);
            }
            if(isset($pre_data['fixed_days'])){
                $data['days'] = json_encode($pre_data['fixed_days']);
            }
            if(isset($pre_data['fixed_work_start'])){
                $data['time'] = json_encode($pre_data['fixed_work_start']. '=>'.$pre_data['fixed_work_end'] );
            }
            $data['all_day'] = $pre_data['all_day'];
            $data['custom_hvolume'] = $pre_data['custom_hvolume'];
        }else{
            $data['type'] = 'custom';
            for ($i=1; $i <= $request['rows_num']; $i++){

                if(isset($pre_data['custom_months_'. $i])){
                    $data['months'][$i] = $pre_data['custom_months_'. $i];
                }
                if(isset($pre_data['custom_days_'. $i])){
                    $data['days'][$i] = $pre_data['custom_days_'. $i];
                }

                if(isset($pre_data['custom_work_start_'. $i])){
                    $data['time'][$i] = $pre_data['custom_work_start_'. $i]. '=>'.$pre_data['custom_work_end_'. $i];
                }
                if(isset($pre_data['custom_hvolume_'. $i])){
                    $data['custom_hvolume'][$i] = $pre_data['custom_hvolume_'. $i];
                }
            }

            $data['months'] = json_encode($data['months']);
            $data['days'] = json_encode($data['days']);
            $data['time'] = json_encode($data['time']);
            $data['custom_hvolume'] = json_encode($data['custom_hvolume']);
        }

        $appointment->update($data);
        return redirect('admin/appointment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }

    public function enableAppointment(Request $request, Appointment $appointment)
    {
        $data['is_active'] = $request['is_active'];
        $appointment->update($data);
        return redirect('/admin/appointment');
    }

    public function getMonthsDays(Request $request){
        if ($request->ajax()){

            $appointment = new Appointment();
            $data['months'] = $appointment->monthsOptions();
            $data['days'] = $appointment->daysOptions();

            return response()->json($data);
        }
    }

    private function validateRequest()
    {
        return request()->validate(
            [
                'label' => 'required',
                'type' => 'required',
                'fixed_days' => 'nullable',
                'custom_days_*' => 'nullable',
                'fixed_months' => 'nullable',
                'custom_months-*' => 'nullable',
                'fixed_work_start' => 'nullable',
                'custom_work_start' => 'nullable',
                'fixed_work_end' => 'nullable',
                'custom_work_end_*' => 'nullable',
                'all_day' => 'nullable',
                'rows_num' => 'nullable',
                'custom_hvolume' => 'nullable',
            ],
            [
                'label.required' => 'Vous devez entrer un label',
                'type.required' => 'Vous devez selectionner un type',
            ]
        );
    }



}
