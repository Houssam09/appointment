<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = 1;
        $clients = User::all();
        return view('admin.clients.index', compact('num', 'clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validateRequest();
        $data['password'] = bcrypt($this->validateRequest()['password']);
        User::create($data);
        return redirect('admin/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $client)
    {
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $client)
    {
        $data = $this->validateRequest();
        if($data['password'] == null){
            unset($data['password']);
        }

        $client->update($data);
        redirect('admin/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return void
     * @throws \Exception
     */
    public function destroy(User $client)
    {
        $client->delete();
    }

    private function validateRequest()
    {
        return request()->validate([
            'name' => 'nullable',
            'code' => 'required',
            'password' => 'nullable',
            'manager' => 'nullable|string',
            'phone' => 'nullable',
            'email' => 'nullable|email|unique:users',
            'hvolume' => 'nullable|not_in:0'
        ],
            [
                'name.required' => 'Le code client est requis',
                'email.unique' => 'cet email existe déja',
            ]);
    }
}
