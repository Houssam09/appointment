<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image, Validator;

class UploadController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'image'           => 'required|image'
            ];

            $validate = Validator::make($request->all(), $rules);

            if ($validate->fails()) {
                $data = [
                    'status'     => 'error',
                    'message'    => 'vous devez choisir un photo !!'
                ];
            }else {
                $folderInput  = $request->input('folder');
                if (!file_exists(public_path('uploads/'.$folderInput))) {
                    mkdir(public_path('uploads/'.$folderInput));
                    chmod(public_path('uploads/'.$folderInput), 0777);
                }
		        $image        = $request->file('image');
		        $exrention    = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
		        $name         = rand(11111, 99999)."_".rand(11111, 99999)."_".time().".".$exrention;
		        $folder       = public_path('uploads/'.$folderInput.'/');
		        $save         = Image::make($image);
		        $save->resize($request->input('width'), $request->input('height'));
		        $save->save($folder.$name);

                $data = [
                    'status'  => 'success',
                    'image'   => asset('uploads/'.$folderInput.'/'.$name)
                ];
            }
            return response()->json($data);
        }    
    }
}
