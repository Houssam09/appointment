<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RdvController extends Controller
{
    public function proposeRDV (Request $request) 
    {
        if ($request->ajax()) {
            $rules = [
                'label'     => 'required',
                'start'     => 'required',
                'end'       => 'required'
            ];

            $validate = Validator::make($request->all(), $rules);

            if ($validate->fails()) {
                $data = [
                    'status'   => 'error',
                    'message'  => 'ERREUR, veuillez réessayer s\'il vous plait!',
                    'errors'   => $validate->errors()
                ];
            }else {
                  $user      = DB::table('users')->where('email',session('user')['email'])->first();
                  $dateStart = new DateTime($request->input('start')); 
                  $dateEnd   = new DateTime($request->input('end'));
                  
                  $rdv = new Rdv(); 
                  $rdv->user_id = $user->id; 
                  $rdv->start   = $dateStart;
                  $rdv->end     = $dateEnd; 
                  $rdv->label   = $request->input('label'); 
                  $rdv->status  = false; 
                  $rdv->presence= false; 
                  $rdv->save(); 
                $data = [
                  'status' => 'success',
                  'message' => 'The update was completely successful!',
                  'url' => null
                ];
            }

            return response()->json($data);
        }
    }
}
