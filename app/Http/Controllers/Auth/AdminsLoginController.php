<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AdminsLoginController extends LoginController
{

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showForm ()
    {
        return view('auth.admin.login');
    }

    public function login(Request $request)
    {
        if (session()->has('user')) {
            redirect()->route('logout');
        }
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        $credentials = ['email' => $request->email, 'password' => $request->password ];
        if(Auth::guard('admin')->attempt($credentials, $request->remember) ){

            $admin = DB::table('admins')->where('email',$request->email)->first();
            $passedData = ['username' => $admin->name, 'email' => $admin->email, 'photo' => $admin->photo];
            session()->put('admin',$passedData); 
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    public function logout (Request $request)
    {
        $this->guard('admin')->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/admin/login');
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
