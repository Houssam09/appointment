<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request, Validator;
use Illuminate\Support\Facades\DB;
use App\Rdv; use DateTime; use Carbon\Carbon;
use App\User;
use App\Events\RdvCanceled;
use App\Events\RdvAprove; 
use App\Events\RdvSupprime;  
class CalendarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rdvs = Rdv::All(); 
        $events = array();
        if (session()->has('user')) {
            $user      = DB::table('users')->where('email',session('user')['email'])->first();
            foreach ($rdvs as $rdv) 
            {
                if (!$rdv->deleted) {                   
                    if ($rdv->user_id == $user->id) {
                        $title = $rdv->label;
                        $color = "#3085d6"; // green 
                        if ($rdv->presence) $color = "#00a65a";
                    }else 
                    {
                        $title = "";
                        $color = "#f56954"; //red
                    }
                    $event = [
                        "eventTitle"      => $title,
                        "start"           => $rdv->start,
                        "end"             => $rdv->end,
                        "backgroundColor" => $color, 
                        "borderColor"     => $color,
                        "id"              => $rdv->id,
                        "company"         => User::find($rdv->user_id)->company,
                    ]; 
                    $events[] = $event;
                }
            }
        }else {
            $color = "#f56954"; //red
            
            foreach ($rdvs as $rdv) 
            {
                if ($rdv->presence) $color = "#00a65a"; //green
                if (!($rdv->deleted)){
                $title = $rdv->label;
                $user  = User::find($rdv->user_id); 
                $event = [
                    "eventTitle"      => $title,
                    "start"           => $rdv->start,
                    "end"             => $rdv->end,
                    "backgroundColor" => $color, 
                    "borderColor"     => $color,
                    "id"              => $rdv->id,
                    "company"         => $user->company,
                ]; 
                $events[] = $event;
            }
            }

        }
        return view('calendar.index',['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function show(Calendar $calendar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function edit(Calendar $calendar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Calendar $calendar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calendar  $calendar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calendar $calendar)
    {
        //
    }
    public function proposeRDV (Request $request) 
    {
        $code = 200; 
        if ($request->ajax()) {
            $rules = [
                'label'     => 'required',
                'start'     => 'required',
                'end'       => 'required'
            ];

            $validate = Validator::make($request->all(), $rules);
            if ($validate->fails()) {
                $data = [
                    'status'   => 'error',
                    'message'  => 'ERREUR, veuillez réessayer s\'il vous plait!',
                    'errors'   => $validate->errors()
                ];
                $code = 400; 
            }else {
                    $DD        = Carbon::createFromFormat('Y m d H:i:s',$request->input('start')); 
                    $ED        = Carbon::createFromFormat('Y m d H:i:s',$request->input('end')); 
                    $user      = DB::table('users')->where('email',session('user')['email'])->first();
                    if ($ED->floatDiffInHours($DD) > ($user->hvolume) ) {
                        $data = [
                            'status'   => 'error',
                            'message'  => 'ERREUR, vous devez choisir un volume horraire inferieure ou égale à '.$user->hvolume .' heurs',
                            'errors'   => 'volume horraire !'
                        ];
                        $code = 400; 
                    } else {
                            $dateStart    = $DD; 
                            $dateEnd      = $ED;
                            $rdv          = new Rdv(); 
                            $rdv->user_id = $user->id; 
                            $rdv->start   = $dateStart;
                            $rdv->end     = $dateEnd; 
                            $rdv->label   = $request->input('label'); 
                            $rdv->status  = false; 
                            $rdv->presence= false; 
                            $rdv->save(); 
                            $data = [
                            'status' => 'success',
                            'message' => 'proposition ajoutée avec réussite!',
                            'url' => null
                            ];
                        }
                }

            return response()->json($data,$code);
        }
    }
    public function setAsPresentRDV(Request $request) {
        $code = 200; 
        if ($request->ajax()) {
            $rules = [
                '_id'     => 'required'
            ];

            $validate = Validator::make($request->all(), $rules);
            if ($validate->fails()) {
                $data = [
                    'status'   => 'error',
                    'message'  => 'ERREUR, veuillez réessayer s\'il vous plait!',
                    'errors'   => $validate->errors()
                ];
                $code = 400; 
            }else {
                $rdv           = Rdv::find($request->input('_id')); 
                $rdv->presence = true; 
                $rdv->save(); 
                event(new RdvAprove($rdv)); // envoie de notification au client que le rendu vous est aprové
                $data = [
                    'status' => 'success',
                    'message' => 'Rendu-vous a été mis a jour avec success!',
                    'url' => null
                    ];
            }

            return response()->json($data,$code);
        }    
    }
    public function softDeleteRDV (Request $request) {
        $code = 200; 
        if ($request->ajax()) {
            $rules = [
                '_id'     => 'required'
            ];

            $validate = Validator::make($request->all(), $rules);
            if ($validate->fails()) {
                $data = [
                    'status'   => 'error',
                    'message'  => 'ERREUR, veuillez réessayer s\'il vous plait!',
                    'errors'   => $validate->errors()
                ];
                $code = 400; 
            }else {
                $rdv = Rdv::find($request->input('_id')); 
                $rdv->deleted   = true; 
                $rdv->save();
                event(new RdvSupprime($rdv)); 
                $data = [
                    'status' => 'success',
                    'message' => 'Rendu-vous a été mis a jour avec success!',
                    'url' => null
                    ];
            }

            return response()->json($data,$code);
        }
    }
    public function deleteRDV (Request $request) {
        $code = 200;

        if ($request->ajax()) {
            $rules = [
                '_id'     => 'required'
            ];

            $validate = Validator::make($request->all(), $rules);
            if ($validate->fails()) {
                $data = [
                    'status'   => 'error',
                    'message'  => 'ERREUR, veuillez réessayer s\'il vous plait!',
                    'errors'   => $validate->errors()
                ];
                $code = 400; 
            }else {
                $rdv = Rdv::find($request->input('_id'));
                if (session()->has('user')) {
                    $user      = DB::table('users')->where('email',session('user')['email'])->first();
                    if ($rdv->user_id != $user->id) {
                        $data = [
                            'status'   => 'error',
                            'message'  => 'ERREUR, vous pouvez annuler que vos rendu-vous!'
                        ];
                        $code = 400; 
                    } else {
                        if ($rdv->presence) {
                           event(new RdvCanceled($rdv)); // envoie notification à l'admin rendu-vous annulé 
                            $data = [
                                'status' => 'success',
                                'message' => 'Rendu-vous annulé, l\'admin sera notifié!',
                                'url' => null
                                ];
                            $code = 200; 
                        } else {
                            $data = [
                                'status' => 'success',
                                'message' => 'Rendu-vous annulé !',
                                'url' => null
                                ];
                            $code = 200; 
                        }
                        $rdv->delete();
                    }
                }
            }
            return response()->json($data,$code);
        }
    }
}
