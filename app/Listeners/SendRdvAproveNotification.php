<?php

namespace App\Listeners;

use App\Events\RdvAprove;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\User;
use App\Notifications\RdvAproveNotification; 
use Illuminate\Support\Facades\Notification;
class SendRdvAproveNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RdvAprove  $event
     * @return void
     */
    public function handle(RdvAprove $event)
    {
        $user =User::find(($event->rdv)->user_id); 
        Notification::send($user, new RdvAproveNotification($event->rdv));
    }
}
