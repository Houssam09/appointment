<?php

namespace App\Listeners;

use App\Events\RdvCanceled;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Admin;
use App\Notifications\RdvCanceledNotification; 
use Illuminate\Support\Facades\Notification;
class SendRdvCanceledNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RdvCanceled  $event
     * @return void
     */
    public function handle(RdvCanceled $event)
    {
        $admins =Admin::all(); 
        Notification::send($admins, new RdvCanceledNotification($event->rdv));
    }
}
