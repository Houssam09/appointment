<?php

namespace App\Listeners;

use App\Events\RdvSupprime;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\User;
use App\Notifications\RdvSupprimeNotification; 
use Illuminate\Support\Facades\Notification;
class SendRdvSupprimeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RdvSupprime  $event
     * @return void
     */
    public function handle(RdvSupprime $event)
    {
        $user =User::find(($event->rdv)->user_id); 
        Notification::send($user, new RdvSupprimeNotification($event->rdv));
    }
}
