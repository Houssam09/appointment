<?php

use Illuminate\Database\Seeder;
use App\Admin; use Carbon\Carbon; 
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin          = new Admin(); 
        $admin->name    = 'admin';
        $admin->role    = 1;
        $admin->email   = 'admin@admin.com';
        $admin->password= bcrypt('10203010'); 
        $admin->email_verified_at = Carbon::now()->subDays(3);
        $admin->created_at = Carbon::now()->subDays(3);
        $admin->updated_at = Carbon::now()->subDays(3);
        $admin->save(); 
    }
}
