const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/css/materialdesignicons.min.css',
    'resources/assets/css/vendor.bundle.base.css',
    'resources/assets/css/datatables.min.css',
    'resources/assets/css/datepicker.min.css',
    'resources/assets/css/select2.min.css',
    'resources/assets/css/sweetalert2.min.css',
    'resources/assets/css/jquery.timepicker.css',
    'resources/assets/css/style.css'

], 'public/css/libs.css');

mix.styles([
    'resources/assets/fullcalendar/packages/core/main.css',
    'resources/assets/fullcalendar/packages/daygrid/main.css',
    'resources/assets/fullcalendar/packages/timegrid/main.css',
    'resources/assets/fullcalendar/packages/bootstrap/main.css',

], 'public/css/fullcalendar.css');

mix.scripts([
    'resources/assets/js/vendor.bundle.base.js',
    'resources/assets/js/hoverable-collapse.js',
    'resources/assets/js/misc.js',
    'resources/assets/js/datatables.min.js',
    'resources/assets/js/datepicker.min.js',
    'resources/assets/js/datepicker-fr.min.js',
    'resources/assets/js/select2.min.js',
    'resources/assets/js/sweetalert2.min.js',
    'resources/assets/js/jquery.timepicker.js',

], 'public/js/libs.js');

mix.scripts([
    'resources/assets/js/script.js'
], 'public/js/script.js');

mix.scripts([
    'resources/assets/fullcalendar/packages/core/main.js',
    'resources/assets/fullcalendar/packages/core/locales/fr.js',
    'resources/assets/fullcalendar/packages/daygrid/main.js',
    'resources/assets/fullcalendar/packages/timegrid/main.js',
    'resources/assets/fullcalendar/packages/interaction/main.js',
    'resources/assets/fullcalendar/packages/bootstrap/main.js',
], 'public/js/fullcalendar.js');

mix.scripts([
    'resources/assets/js/moment/moment.js',
    'resources/assets/js/moment/moment-fr.js',
], 'public/js/moment.js');

mix.scripts([
    'resources/assets/js/calendar.js',
], 'public/js/calendar.js');
