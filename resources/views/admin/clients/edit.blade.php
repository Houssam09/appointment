@extends('layouts.index', ['title' => 'تعديل الزبون', 'class' => 'pg-title bg-gradient-success' ])

@section('content')
    {!! Form::model($client, ['method' => 'PATCH', 'action' => ['Admin\ClientsController@update', $client->id]]) !!}
        @include('admin.clients.form')
    {!! Form::close() !!}

@endsection
