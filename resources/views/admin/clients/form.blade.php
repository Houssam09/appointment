<div class="form-group">
    {!! Form::label('name', 'Nom de client:') !!}
    {!! Form::text('name', old('name'), ['class'=>'form-control client-name '.($errors->has('name') ? 'is-invalid':'')]) !!}
        <span class="invalid-feedback">{{$errors->first('name')}}</span>
</div>
<div class="form-group">
    {!! Form::label('code', 'Code de client:') !!}
    {!! Form::text('code', old('code'), ['class'=>'form-control '.($errors->has('code') ? 'is-invalid':'')]) !!}
    <span class="invalid-feedback">{{$errors->first('code')}}</span>
</div>
<div class="form-group">
    {!! Form::label('manager', 'Nom de gérant:') !!}
    {!! Form::text('manager', old('manager'), ['class'=>'form-control '.($errors->has('manager') ? 'is-invalid':'')]) !!}
    <span class="invalid-feedback">{{$errors->first('manager')}}</span>
</div>
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', old('email'), ['class'=>'form-control '.($errors->has('email') ? 'is-invalid':'')]) !!}
    <span class="invalid-feedback">{{$errors->first('email')}}</span>
</div>
<div class="form-group">
    {!! Form::label('password', 'Mot de passe:') !!}
    <input type="text" name="password" class="form-control {{$errors->has('password') ? 'is-invalid':''}}" value="">
    <span class="invalid-feedback">{{$errors->first('password')}}</span>
</div>
<div class="form-group">
    {!! Form::label('phone', 'N° de téléphone:') !!}
    {!! Form::text('phone', old('phone'), ['class'=>'form-control '.($errors->has('phone') ? 'is-invalid':'')]) !!}
    <span class="invalid-feedback">{{$errors->first('phone')}}</span>
</div>
<div class="form-group">
    {!! Form::label('hvolume', 'Volume horaire:') !!}
    {!! Form::text('hvolume', old('hvolume'), ['class'=>'form-control '.($errors->has('hvolume') ? 'is-invalid':'')]) !!}
    <span class="invalid-feedback">{{$errors->first('hvolume')}}</span>
</div>

<div class="form-group">
    {!! Form::submit('Ajouter', ['class'=>'btn btn-sm btn-gradient-success']) !!}
</div>
