@extends('layouts.index', ['title' => 'Ajouter un nouveau client', 'class' => 'pg-title bg-gradient-success' ])

@section('content')
    {!! Form::open(['method' => 'POST', 'action' => 'Admin\ClientsController@store']) !!}
        @include('admin.clients.form')
    {!! Form::close() !!}


@endsection
