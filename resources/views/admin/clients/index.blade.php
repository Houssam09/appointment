@extends('layouts.app')
@section('styles')
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

  <style>
    
    .multisteps-form__progress {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(0, 1fr));
    }
    
    .multisteps-form__progress-btn {
        transition-property: all;
        transition-duration: 0.15s;
        transition-timing-function: linear;
        transition-delay: 0s;
        position: relative;
        padding-top: 20px;
        color: rgba(108, 117, 125, 0.7);
        text-indent: -9999px;
        border: none;
        background-color: transparent;
        outline: none !important;
        cursor: pointer;
    }
    
    @media (min-width: 500px) {
        .multisteps-form__progress-btn {
        text-indent: 0;
        }
    }
    
    .multisteps-form__progress-btn:before {
        position: absolute;
        top: 0;
        left: 50%;
        display: block;
        width: 13px;
        height: 13px;
        content: '';
        -webkit-transform: translateX(-50%);
                transform: translateX(-50%);
        transition: all 0.15s linear 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
        transition: all 0.15s linear 0s, transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
        transition: all 0.15s linear 0s, transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
        border: 2px solid currentColor;
        border-radius: 50%;
        background-color: #fff;
        box-sizing: border-box;
        z-index: 3;
    }
    
    .multisteps-form__progress-btn:after {
        position: absolute;
        top: 5px;
        left: calc(-50% - 13px / 2);
        transition-property: all;
        transition-duration: 0.15s;
        transition-timing-function: linear;
        transition-delay: 0s;
        display: block;
        width: 100%;
        height: 2px;
        content: '';
        background-color: currentColor;
        z-index: 1;
    }
    
    .multisteps-form__progress-btn:first-child:after {
        display: none;
    }
    
    .multisteps-form__progress-btn.js-active {
        color: #007bff;
    }
    
    .multisteps-form__progress-btn.js-active:before {
        -webkit-transform: translateX(-50%) scale(1.2);
                transform: translateX(-50%) scale(1.2);
        background-color: currentColor;
    }
    
    .multisteps-form__form {
        position: relative;
    }
    
    .multisteps-form__panel {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 0;
        opacity: 0;
        visibility: hidden;
    }
    
    .multisteps-form__panel.js-active {
        height: auto;
        opacity: 1;
        visibility: visible;
    }
    .multisteps-form__panel[data-animation="scaleIn"] {
        -webkit-transform: scale(0.9);
                transform: scale(0.9);
    }
    
    .multisteps-form__panel[data-animation="scaleIn"].js-active {
        transition-property: all;
        transition-duration: 0.2s;
        transition-timing-function: linear;
        transition-delay: 0s;
        -webkit-transform: scale(1);
                transform: scale(1);
    }
  </style>
@endsection
@section('content')
     <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Clients</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Clients</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="d-inline card-title"> Liste des clients et leurs informations</h3>
                  <button type="button" style="float: right;" class="d-inline btn  btn-outline-primary btn-sm" data-toggle="modal" data-target="#addClientForm">Ajouter Client</button>
                  <div class="modal fade" id="addClientForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ajouter Client</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                            </div>
                            <div class="modal-body">
                              <div class="multisteps-form">
                                <!--progress bar-->
                                <div class="row">
                                  <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
                                    <div class="multisteps-form__progress">
                                      <button class="multisteps-form__progress-btn js-active" type="button" title="User Info">Informations Client</button>
                                      <button class="multisteps-form__progress-btn" type="button" title="Address">Addresse</button>
                                      <button class="multisteps-form__progress-btn" type="button" title="Order Info">Order Info</button>
                                      <button class="multisteps-form__progress-btn" type="button" title="Comments">Comments        </button>
                                    </div>
                                  </div>
                                </div>
                                <!--form panels-->
                                <div class="row">
                                  <div class="col-12 col-lg-8 m-auto">
                                    <form class="multisteps-form__form">
                                      <!--single form panel-->
                                      <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                        <h3 class="multisteps-form__title">Your User Info</h3>
                                        <div class="multisteps-form__content">
                                          <div class="form-row mt-4">
                                            <div class="col-12 col-sm-6">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="First Name"/>
                                            </div>
                                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="Last Name"/>
                                            </div>
                                          </div>
                                          <div class="form-row mt-4">
                                            <div class="col-12 col-sm-6">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="Login"/>
                                            </div>
                                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                              <input class="multisteps-form__input form-control" type="email" placeholder="Email"/>
                                            </div>
                                          </div>
                                          <div class="form-row mt-4">
                                            <div class="col-12 col-sm-6">
                                              <input class="multisteps-form__input form-control" type="password" placeholder="Password"/>
                                            </div>
                                            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                              <input class="multisteps-form__input form-control" type="password" placeholder="Repeat Password"/>
                                            </div>
                                          </div>
                                          <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                                          </div>
                                        </div>
                                      </div>
                                      <!--single form panel-->
                                      <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                        <h3 class="multisteps-form__title">Your Address</h3>
                                        <div class="multisteps-form__content">
                                          <div class="form-row mt-4">
                                            <div class="col">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="Address 1"/>
                                            </div>
                                          </div>
                                          <div class="form-row mt-4">
                                            <div class="col">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="Address 2"/>
                                            </div>
                                          </div>
                                          <div class="form-row mt-4">
                                            <div class="col-12 col-sm-6">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="City"/>
                                            </div>
                                            <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                                              <select class="multisteps-form__select form-control">
                                                <option selected="selected">State...</option>
                                                <option>...</option>
                                              </select>
                                            </div>
                                            <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                                              <input class="multisteps-form__input form-control" type="text" placeholder="Zip"/>
                                            </div>
                                          </div>
                                          <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                                            <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                                          </div>
                                        </div>
                                      </div>
                                      <!--single form panel-->
                                      <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                        <h3 class="multisteps-form__title">Your Order Info</h3>
                                        <div class="multisteps-form__content">
                                          <div class="row">
                                            <div class="col-12 col-md-6 mt-4">
                                              <div class="card shadow-sm">
                                                <div class="card-body">
                                                  <h5 class="card-title">Item Title</h5>
                                                  <p class="card-text">Small and nice item description</p><a class="btn btn-primary" href="#" title="Item Link">Item Link</a>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="col-12 col-md-6 mt-4">
                                              <div class="card shadow-sm">
                                                <div class="card-body">
                                                  <h5 class="card-title">Item Title</h5>
                                                  <p class="card-text">Small and nice item description</p><a class="btn btn-primary" href="#" title="Item Link">Item Link</a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="button-row d-flex mt-4 col-12">
                                              <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                                              <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <!--single form panel-->
                                      <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                                        <h3 class="multisteps-form__title">Additional Comments</h3>
                                        <div class="multisteps-form__content">
                                          <div class="form-row mt-4">
                                            <textarea class="multisteps-form__textarea form-control" placeholder="Additional Comments and Requirements"></textarea>
                                          </div>
                                          <div class="button-row d-flex mt-4">
                                            <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                                            <button class="btn btn-success ml-auto" type="button" title="Send">Send</button>
                                          </div>
                                        </div>
                                      </div>
                                    </form>
                                   </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>           
                </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 4.0
                    </td>
                    <td>Win 95+</td>
                    <td> 4</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 5.0
                    </td>
                    <td>Win 95+</td>
                    <td>5</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 5.5
                    </td>
                    <td>Win 95+</td>
                    <td>5.5</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 6
                    </td>
                    <td>Win 98+</td>
                    <td>6</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>Internet Explorer 7</td>
                    <td>Win XP SP2+</td>
                    <td>7</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>AOL browser (AOL desktop)</td>
                    <td>Win XP</td>
                    <td>6</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Firefox 1.0</td>
                    <td>Win 98+ / OSX.2+</td>
                    <td>1.7</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Firefox 1.5</td>
                    <td>Win 98+ / OSX.2+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Firefox 2.0</td>
                    <td>Win 98+ / OSX.2+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Firefox 3.0</td>
                    <td>Win 2k+ / OSX.3+</td>
                    <td>1.9</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Camino 1.0</td>
                    <td>OSX.2+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Camino 1.5</td>
                    <td>OSX.3+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Netscape 7.2</td>
                    <td>Win 95+ / Mac OS 8.6-9.2</td>
                    <td>1.7</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Netscape Browser 8</td>
                    <td>Win 98SE+</td>
                    <td>1.7</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Netscape Navigator 9</td>
                    <td>Win 98+ / OSX.2+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.0</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.1</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1.1</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.2</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1.2</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.3</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1.3</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.4</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1.4</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.5</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1.5</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.6</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>1.6</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.7</td>
                    <td>Win 98+ / OSX.1+</td>
                    <td>1.7</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Mozilla 1.8</td>
                    <td>Win 98+ / OSX.1+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Seamonkey 1.1</td>
                    <td>Win 98+ / OSX.2+</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Gecko</td>
                    <td>Epiphany 2.20</td>
                    <td>Gnome</td>
                    <td>1.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>Safari 1.2</td>
                    <td>OSX.3</td>
                    <td>125.5</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>Safari 1.3</td>
                    <td>OSX.3</td>
                    <td>312.8</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>Safari 2.0</td>
                    <td>OSX.4+</td>
                    <td>419.3</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>Safari 3.0</td>
                    <td>OSX.4+</td>
                    <td>522.1</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>OmniWeb 5.5</td>
                    <td>OSX.4+</td>
                    <td>420</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>iPod Touch / iPhone</td>
                    <td>iPod</td>
                    <td>420.1</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Webkit</td>
                    <td>S60</td>
                    <td>S60</td>
                    <td>413</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 7.0</td>
                    <td>Win 95+ / OSX.1+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 7.5</td>
                    <td>Win 95+ / OSX.2+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 8.0</td>
                    <td>Win 95+ / OSX.2+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 8.5</td>
                    <td>Win 95+ / OSX.2+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 9.0</td>
                    <td>Win 95+ / OSX.3+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 9.2</td>
                    <td>Win 88+ / OSX.3+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera 9.5</td>
                    <td>Win 88+ / OSX.3+</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Opera for Wii</td>
                    <td>Wii</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Nokia N800</td>
                    <td>N800</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Presto</td>
                    <td>Nintendo DS browser</td>
                    <td>Nintendo DS</td>
                    <td>8.5</td>
                    <td>C/A<sup>1</sup></td>
                  </tr>
                  <tr>
                    <td>KHTML</td>
                    <td>Konqureror 3.1</td>
                    <td>KDE 3.1</td>
                    <td>3.1</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>KHTML</td>
                    <td>Konqureror 3.3</td>
                    <td>KDE 3.3</td>
                    <td>3.3</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>KHTML</td>
                    <td>Konqureror 3.5</td>
                    <td>KDE 3.5</td>
                    <td>3.5</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Tasman</td>
                    <td>Internet Explorer 4.5</td>
                    <td>Mac OS 8-9</td>
                    <td>-</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Tasman</td>
                    <td>Internet Explorer 5.1</td>
                    <td>Mac OS 7.6-9</td>
                    <td>1</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>Tasman</td>
                    <td>Internet Explorer 5.2</td>
                    <td>Mac OS 8-X</td>
                    <td>1</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>NetFront 3.1</td>
                    <td>Embedded devices</td>
                    <td>-</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>NetFront 3.4</td>
                    <td>Embedded devices</td>
                    <td>-</td>
                    <td>A</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>Dillo 0.8</td>
                    <td>Embedded devices</td>
                    <td>-</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>Links</td>
                    <td>Text only</td>
                    <td>-</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>Lynx</td>
                    <td>Text only</td>
                    <td>-</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>IE Mobile</td>
                    <td>Windows Mobile 6</td>
                    <td>-</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>Misc</td>
                    <td>PSP browser</td>
                    <td>PSP</td>
                    <td>-</td>
                    <td>C</td>
                  </tr>
                  <tr>
                    <td>Other browsers</td>
                    <td>All others</td>
                    <td>-</td>
                    <td>-</td>
                    <td>U</td>
                  </tr>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  
  <!-- page script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
        "language": {
              "lengthMenu": "Afficher _MENU_ elements par page",
              "zeroRecords": "Aucun enregistrement à afficher",
              "info": "Affichage de la page _PAGE_ de _PAGES_",
              "infoEmpty": "aucun enregistrement disponible",
              "infoFiltered": "(filtré de _MAX_ enregistrements)",
              "search":         "chercher:",
              "paginate": {
                  "first":      "première",
                  "last":       "dernière",
                  "next":       "suivante",
                  "previous":   "précédente"
                },
          },
      });
    });
  </script>
  <script>
      //DOM elements
    const DOMstrings = {
      stepsBtnClass: 'multisteps-form__progress-btn',
      stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
      stepsBar: document.querySelector('.multisteps-form__progress'),
      stepsForm: document.querySelector('.multisteps-form__form'),
      stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
      stepFormPanelClass: 'multisteps-form__panel',
      stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
      stepPrevBtnClass: 'js-btn-prev',
      stepNextBtnClass: 'js-btn-next' };
    
    
    //remove class from a set of items
    const removeClasses = (elemSet, className) => {
    
      elemSet.forEach(elem => {
    
        elem.classList.remove(className);
    
      });
    
    };
    
    //return exect parent node of the element
    const findParent = (elem, parentClass) => {
    
      let currentNode = elem;
    
      while (!currentNode.classList.contains(parentClass)) {
        currentNode = currentNode.parentNode;
      }
    
      return currentNode;
    
    };
    
    //get active button step number
    const getActiveStep = elem => {
      return Array.from(DOMstrings.stepsBtns).indexOf(elem);
    };
    
    //set all steps before clicked (and clicked too) to active
    const setActiveStep = activeStepNum => {
    
      //remove active state from all the state
      removeClasses(DOMstrings.stepsBtns, 'js-active');
    
      //set picked items to active
      DOMstrings.stepsBtns.forEach((elem, index) => {
    
        if (index <= activeStepNum) {
          elem.classList.add('js-active');
        }
    
      });
    };
    
    //get active panel
    const getActivePanel = () => {
    
      let activePanel;
    
      DOMstrings.stepFormPanels.forEach(elem => {
    
        if (elem.classList.contains('js-active')) {
    
          activePanel = elem;
    
        }
    
      });
    
      return activePanel;
    
    };
    
    //open active panel (and close unactive panels)
    const setActivePanel = activePanelNum => {
    
      //remove active class from all the panels
      removeClasses(DOMstrings.stepFormPanels, 'js-active');
    
      //show active panel
      DOMstrings.stepFormPanels.forEach((elem, index) => {
        if (index === activePanelNum) {
    
          elem.classList.add('js-active');
    
          setFormHeight(elem);
    
        }
      });
    
    };
    
    //set form height equal to current panel height
    const formHeight = activePanel => {
    
      const activePanelHeight = activePanel.offsetHeight;
    
      DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;
    
    };
    
    const setFormHeight = () => {
      const activePanel = getActivePanel();
    
      formHeight(activePanel);
    };
    
    //STEPS BAR CLICK FUNCTION
    DOMstrings.stepsBar.addEventListener('click', e => {
    
      //check if click target is a step button
      const eventTarget = e.target;
    
      if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
        return;
      }
    
      //get active button step number
      const activeStep = getActiveStep(eventTarget);
    
      //set all steps before clicked (and clicked too) to active
      setActiveStep(activeStep);
    
      //open active panel
      setActivePanel(activeStep);
    });
    
    //PREV/NEXT BTNS CLICK
    DOMstrings.stepsForm.addEventListener('click', e => {
    
      const eventTarget = e.target;
    
      //check if we clicked on `PREV` or NEXT` buttons
      if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)))
      {
        return;
      }
    
      //find active panel
      const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);
    
      let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);
    
      //set active step and active panel onclick
      if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
        activePanelNum--;
    
      } else {
    
        activePanelNum++;
    
      }
    
      setActiveStep(activePanelNum);
      setActivePanel(activePanelNum);
    
    });
    
    //SETTING PROPER FORM HEIGHT ONLOAD
    window.addEventListener('load', setFormHeight, false);
    
    //SETTING PROPER FORM HEIGHT ONRESIZE
    window.addEventListener('resize', setFormHeight, false);
  </script>
@endsection1