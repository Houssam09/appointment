@extends('layouts.index', ['title' => 'Modifier plage', 'class' => 'pg-title bg-gradient-danger' ])

@section('content')
    {!! Form::model($appointment, ['method' => 'PATCH', 'action' => ['Admin\AppointmentsController@update', $appointment->id]]) !!}
    @include('admin.appointments.edit-form')
    {!! Form::close() !!}


@endsection
