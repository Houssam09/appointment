@extends('layouts.index', ['title' => 'Plages', 'class' => 'pg-title bg-gradient-danger' ])

@section('content')
    <div class="link-add-box">
        <a href="{{route('appointment.create')}}" class="btn btn-gradient-primary">Ajouter nouvelle plage</a>
    </div>
    <table id="myTable" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">label</th>
            <th scope="col">type</th>
            <th scope="col">Mois</th>
            <th scope="col">عمليات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($appointments as $appointment)
            <tr>
                <th scope="row">{{$num++}}</th>
                <td>{{$appointment->label}}</td>
                <td>{{$appointment->type == 'fixed' ? 'fixe' : 'personaliser'}}</td>
                <td>
                    @if($appointment->type == 'fixed')
                        @foreach(json_decode($appointment->months) as $month)
                            {{$appointment->monthsOptions()[$month]}}
                        @endforeach
                    @else
                        @php
                            $data_months = (array)json_decode($appointment->months);
                            $data_days = (array)json_decode($appointment->days);
                            $data_time = (array)json_decode($appointment->time);

                        @endphp
                        @for($i=1; $i<=count($data_months); $i++)
                            @for($j=0; $j< count($data_months[$i]); $j++)
                                {{ isset($data_months[$i][$j]) ? $appointment->monthsOptions()[$data_months[$i][$j]] : ' ' }}
                                {{isset($data_days[$i][$j]) ? $appointment->daysOptions()[$data_days[$i][$j]] : ' ' }}
                                {{ $data_time[$i] }}
                                {!! '<br>' !!}
                            @endfor
                        @endfor
                    @endif
                </td>
                <td class="dropdown">
                    <a href="#" class="delete btn btn-sm btn-danger" data-id="{{$appointment->id}}" data-url="{{route('appointment.destroy', $appointment->id)}}" data-success="تم حذف الفئة بنجاح!" >Supprimer</a>
                    <a href="{{route('appointment.edit', $appointment->id)}}" class="btn btn-sm btn-success">Modifier</a>
                    @if($appointment->is_active == 1)
                        <a href="{{route('appointment.edit', $appointment->id)}}" class="btn btn-sm btn-dark">reactiver</a>
                    @else
                        <a href="{{route('appointment.edit', $appointment->id)}}" class="btn btn-sm btn-secondary">désactiver</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

