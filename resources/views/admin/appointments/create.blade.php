@extends('layouts.index', ['title' => 'Ajouter plage', 'class' => 'pg-title bg-gradient-danger' ])

@section('content')
    {!! Form::open(['method' => 'POST', 'action' => 'Admin\AppointmentsController@store']) !!}
    @include('admin.appointments.form')
    {!! Form::close() !!}


@endsection
