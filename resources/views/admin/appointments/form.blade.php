<div class="form-group">
    <input type="checkbox" name="type" value="fixed" > Fixe
    <input type="checkbox" name="type" value="custom"> Personaliser
</div>

<div>
    <div class="form-group">
        {!! Form::label('fixed_label', 'Label:') !!}
        <input type="text" name="fixed_label" class="form-control {{$errors->has('fixed_label') ? 'is-invalid':''}}">
        <span class="invalid-feedback">{{$errors->first('fixed_label')}}</span>
    </div>
    <div class="form-group">
        <label for="">Les mois</label>
        <div class="row">
            @foreach($appointment->monthsOptions() as $key => $month)
                <div class="col-3">
                    <input type="checkbox" name="fixed_months[]" value="{{$key}}"> {{ $month }}
                </div>
            @endforeach
        </div>
    </div>

    <div class="form-group">
        <label for="">Les jours</label>
        <div class="row">
            @foreach($appointment->daysOptions() as $key => $day)
                <div class="col-3">
                    <input type="checkbox" name="fixed_days[]" value="{{$key}}" @if(!empty($appointment->days) && in_array($key, (array)json_decode($appointment->days))) checked @endif > {{ $day }}
                </div>
            @endforeach
        </div>

    </div>
    <div class="form-group">
        <input type="radio" name="all_day" value="true"> Toute la journée
        <input type="radio" name="all_day" value="false"> Pesonaliser
    </div>

    <div class="form-group">
        {!! Form::label('fixed_work_start', 'Heure de debut:') !!}
        <input type="text" name="fixed_work_start" class="form-control {{$errors->has('fixed_work_start') ? 'is-invalid':''}}">
        <span class="invalid-feedback">{{$errors->first('fixed_work_start')}}</span>
    </div>
    <div class="form-group">
        {!! Form::label('fixed_work_end', 'Heure de fin:') !!}
        <input type="text" name="fixed_work_end" class="form-control {{$errors->has('fixed_work_end') ? 'is-invalid':''}}">
        <span class="invalid-feedback">{{$errors->first('fixed_work_end')}}</span>
    </div>

    <input type="radio"> Volume Pesonaliser?
    <div class="form-group">
        {!! Form::label('custom_hvolume', 'Volume horaire:') !!}
        <input type="text" name="custom_hvolume" class="form-control {{$errors->has('custom_hvolume') ? 'is-invalid':''}}">
        <span class="invalid-feedback">{{$errors->first('custom_hvolume')}}</span>
    </div>
</div>


<hr>

<div>
    <div class="form-group">
        {!! Form::label('custom_label', 'Label:') !!}
        <input type="text" name="custom_label" class="form-control {{$errors->has('custom_label') ? 'is-invalid':''}}">
        <span class="invalid-feedback">{{$errors->first('custom_label')}}</span>
    </div>

    <div class="row custom-row">
        <div class="col-3">
            <select name="custom_months_1[]" class="form-control select-js" multiple>
                @foreach($appointment->monthsOptions() as $key => $month)
                    <option value="{{$key}}">{{$month}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <select name="custom_days_1[]" class="form-control select-js" multiple>
                @foreach($appointment->daysOptions() as $key => $day)
                    <option value="{{$key}}">{{$day}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col-6">
                    {!! Form::text('custom_work_start_1', old('custom_work_start'), ['class'=>'form-control '.($errors->has('custom_work_start') ? 'is-invalid':''), 'placeholder' => 'Heure de début']) !!}
                    <span class="invalid-feedback">{{$errors->first('custom_work_start')}}</span>
                </div>
                <div class="col-6">
                    {!! Form::text('custom_work_end_1', old('custom_work_end'), ['class'=>'form-control '.($errors->has('custom_work_end') ? 'is-invalid':''), 'placeholder' => 'Heure de fin']) !!}
                    <span class="invalid-feedback">{{$errors->first('custom_work_end')}}</span>
                </div>
            </div>
        </div>
        <div class="col-2">
            <input type="text" name="custom_hvolume_1" class="form-control {{$errors->has('custom_hvolume') ? 'is-invalid':''}}" placeholder="volume horaire">
            <span class="invalid-feedback">{{$errors->first('custom_hvolume')}}</span>
        </div>
        <div class="col-1">
            <button id="addRow" type="button" class="btn btn-sm btn-success">+</button>
        </div>
    </div>

    <div><input type="hidden" class="rows-num" name="rows_num" value="1"></div>
</div>



<div class="form-group">
    {!! Form::submit('أضف', ['class'=>'btn btn-primary']) !!}
</div>


