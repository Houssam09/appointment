<input type="hidden" name="type" value="{{($appointment->type == 'fixed')? 'fixed' : 'custom'}}">
@if($appointment->type == 'fixed')
<div>
    <div class="form-group">
        {!! Form::label('label', 'Label:') !!}
        <input type="text" name="label" class="form-control {{$errors->has('label') ? 'is-invalid':''}}" value="{{isset($appointment->label) ? $appointment->label : '' }}">
        <span class="invalid-feedback">{{$errors->first('label')}}</span>
    </div>
    <div class="form-group">
        <label for="">Les mois</label>
        <div class="row">
            @foreach($appointment->monthsOptions() as $key => $month)
                <div class="col-3">
                    <input type="checkbox" name="fixed_months[]" value="{{$key}}" @if(!empty($appointment->months) && in_array($key, (array)json_decode($appointment->months))) checked @endif> {{ $month }}
                </div>
            @endforeach
        </div>
    </div>

    <div class="form-group">
        <label for="">Les jours</label>
        <div class="row">
            @foreach($appointment->daysOptions() as $key => $day)
                <div class="col-3">
                    <input type="checkbox" name="fixed_days[]" value="{{$key}}" @if(!empty($appointment->days) && in_array($key, (array)json_decode($appointment->days))) checked @endif > {{ $day }}
                </div>
            @endforeach
        </div>

    </div>
    <div class="form-group">
        <input type="radio" name="all_day" value="true" {{($appointment->all_day == 'true')? 'checked' : ''}}> Toute la journée
        <input type="radio" name="all_day" value="false" {{($appointment->all_day == 'false')? 'checked' : ''}}> Pesonaliser
    </div>
    @if($appointment->time)
        @php
            $time = explode('=>', json_decode($appointment->time));
            $start = $time[0];
            $end = $time[1];
        @endphp
    @endif
    <div class="form-group">
        {!! Form::label('fixed_work_start', 'Heure de debut:') !!}
        <input type="text" name="fixed_work_start" class="form-control {{$errors->has('fixed_work_start') ? 'is-invalid':''}}" value="{{isset($start) ? $start : '' }}">
        <span class="invalid-feedback">{{$errors->first('fixed_work_start')}}</span>
    </div>
    <div class="form-group">
        {!! Form::label('fixed_work_end', 'Heure de fin:') !!}
        <input type="text" name="fixed_work_end" class="form-control {{$errors->has('fixed_work_end') ? 'is-invalid':''}}" value="{{isset($end) ? $end : '' }}">
        <span class="invalid-feedback">{{$errors->first('fixed_work_end')}}</span>
    </div>
    <input type="radio" {{isset($appointment->custom_hvolume) ? 'checked' : '' }}> Volume Pesonaliser?
    <div class="form-group">
        {!! Form::label('custom_hvolume', 'Volume horaire:') !!}
        <input type="text" name="custom_hvolume" class="form-control {{$errors->has('custom_hvolume') ? 'is-invalid':''}}" value="{{isset($appointment->custom_hvolume) ? $appointment->custom_hvolume : '' }}">
        <span class="invalid-feedback">{{$errors->first('custom_hvolume')}}</span>
    </div>
</div>
@endif

@if($appointment->type == 'custom')
<div>

    <div class="form-group">
        {!! Form::label('label', 'Label:') !!}
        <input type="text" name="label" class="form-control {{$errors->has('label') ? 'is-invalid':''}}" value="{{isset($appointment->label) ? $appointment->label : '' }}">
        <span class="invalid-feedback">{{$errors->first('label')}}</span>
    </div>

    @php
        $rows_num = count((array)json_decode($appointment->months));
        $months = (array)json_decode($appointment->months);
        $days = (array)json_decode($appointment->days);
        $time = (array)json_decode($appointment->time);
        $custom_hvolume = (array)json_decode($appointment->custom_hvolume);
    @endphp

    @for($i=1; $i <= $rows_num; $i++)
    <div class="row custom-row">
        <div class="col-3">
            <select name="custom_months_{{$i}}[]" class="form-control select-js" multiple>
                @foreach($appointment->monthsOptions() as $key => $month)
                    @foreach($months[$i] as $k => $m)
                        @if($m == $key) <option value="{{$m}}" selected>{{$appointment->monthsOptions()[$m]}}</option>  @break @endif
                    @endforeach
                    <option value="{{$key}}">{{$month}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <select name="custom_days_{{$i}}[]" class="form-control select-js" multiple>
                @foreach($appointment->daysOptions() as $key => $day)
                    @foreach($days[$i] as $k => $d)
                        @if($d == $key) <option value="{{$d}}" selected>{{$appointment->daysOptions()[$d]}}</option>  @break @endif
                    @endforeach
                    <option value="{{$key}}">{{$day}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <div class="row">
                @php
                        $custom_time = explode('=>', $time[$i]);
                        $start = $custom_time[0];
                        $end = $custom_time[1];

                @endphp
                <div class="col-6">
                    <input type="text" name="custom_work_start_{{$i}}" class="form-control {{$errors->has('custom_work_start') ? 'is-invalid':''}}" value="{{isset($start) ? $start : '' }}">                    <span class="invalid-feedback">{{$errors->first('custom_work_start')}}</span>
                </div>
                <div class="col-6">
                    <input type="text" name="custom_work_end_{{$i}}" class="form-control {{$errors->has('fixed_work_end') ? 'is-invalid':''}}" value="{{isset($end) ? $end : '' }}">
                    <span class="invalid-feedback">{{$errors->first('custom_work_end')}}</span>
                </div>
            </div>
        </div>
        <div class="col-2">
            <input type="text" name="custom_hvolume_{{$i}}" class="form-control {{$errors->has('custom_hvolume') ? 'is-invalid':''}}" placeholder="volume horaire" value="{{isset($custom_hvolume[$i]) ? $custom_hvolume[$i] : '' }}">
            <span class="invalid-feedback">{{$errors->first('custom_hvolume')}}</span>
        </div>
        @if($rows_num - $i ==0)
        <div class="col-1">
            <button id="addRow" type="button" class="btn btn-sm btn-success">+</button>
        </div>
        @else
            <div class="col-1">
                <button id="removeRow" type="button" class="btn btn-sm btn-danger">-</button>
            </div>
        @endif
    </div>
    @endfor

    <div><input type="hidden" class="rows-num" name="rows_num" value="{{$rows_num}}"></div>
</div>

@endif

<div class="form-group">
    {!! Form::submit('أضف', ['class'=>'btn btn-primary']) !!}
</div>


