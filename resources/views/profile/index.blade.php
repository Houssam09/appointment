@extends('layouts.app')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert2/sweetalert2.css') }}?v={{ config('app.version') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/toastr/toastr.css') }}?v={{ config('app.version') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-imgupload.css') }}">
@endsection
 
@section('script')
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.js') }}?v={{ config('app.version') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/toastr/toastr.js') }}?v={{ config('app.version') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap-imgupload.js')}}"></script>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">paramétres de connexion</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Profil</li>
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card bg-transparent">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form class="form" method="post" data-url="{{ $data['route'] }}">
                                        @csrf
                                        <div class="form-group" id="name">
                                            <label>Nom</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                </div>
                                                <inputtype="text" placeholder="nom" class="form-control" name="name" autofocus="" value="{{ $data['name'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group" id="email">
                                            <label> E-mail</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                                </div>
                                                <input type="email" class="form-control" placeholder="Email" name="email" autofocus="" value="{{ $data['email'] }}">
                                            </div>
                                        </div>
                                        <div class="form-group" id="project_image">
                                        <label>Photo de profil</label>
                                    <div class="fileupload">
                                        <div class="thumbnail">
                                            <img class="img-thumbnail bg-white rounded" src="{{ $data['photo']== "" ? asset('images/avatar.png') : asset($data['photo']) }}" />
                                        </div>
                                        <span class="btn btn-file btn-primary">
                                            <i class="ft-upload"></i>
                                            Changer photo
                                            <input type="file" id="imageinput" data-id="user_photo" data-url="{{ route('upload') }}" data-text=" change image" data-folder="profiles" />
                                        </span>
                                        <div class="position-relative has-icon-left">
                                            <input type="text" class="form-control" id="user_photo" name="photo" value="{{ $data['photo'] }}" readonly="">
                                            <div class="form-control-position">
                                                <i class="fa fa-image"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        <div class="form-group">
                                            <button type="button" id="submit" class="btn btn-primary btn-block mt-1">
                                                <i class="ft-check-square"></i>
                                                save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection