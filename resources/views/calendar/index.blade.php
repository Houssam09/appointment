@extends('layouts.app')
@section('styles')
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- fullCalendar -->
  <link rel="stylesheet" href="{{asset('plugins/fullcalendar/main.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/fullcalendar-daygrid/main.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/fullcalendar-timegrid/main.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/fullcalendar-bootstrap/main.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/toastr/toastr.css') }}?v={{ config('app.version') }}">
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Calendar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Calendar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="d-flex flex-row justify-content-center">
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card card-primary">
              <div class="card-body p-0">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <div id="calendarModal" class="modal fade">
      <div class="modal-dialog">
          <div  class="modal-content">
              <div dir="rtl" class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Rdv Details</h4>
              </div>
              <div id="modalBody" class="modal-body">
              <h4 id="modalTitle" class="modal-title"></h4>
              <div id="modalWhen" style="margin-top:5px;"></div>
              </div>
              <input type="hidden" id="eventID"/>
              <input type="hidden" id="calendarEventID"/>
              <div class="modal-footer">
                @if(session()->has('admin'))
                  <button  class="btn  btn-outline-secondary" data-dismiss="modal" >Annuler</button>
                  <button type="submit" class="btn btn-outline-success" id="proveButton">Présent</button>
                  <button type="submit" class="btn btn-outline-danger" id="deleteButton">Supprimer</button>
                @endif
                @if(session()->has('user'))
                <button type="submit" class="btn btn-outline-secondary btn-sm" id="annulerbutton">Annuler rendu-vous</button>
                @endif
              </div>
          </div>
      </div>
	  </div>
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
  <!-- jQuery UI -->
  <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{asset('dist/js/demo.js')}}"></script>
  <!-- fullCalendar 2.2.5 -->
  <script src="{{asset('plugins/moment/moment-with-locales.js')}}"></script>
  <script src="{{asset('plugins/fullcalendar/main.min.js')}}"></script>
  <script src="{{asset('plugins/fullcalendar/locales/fr.js')}}"></script>
  <script src="{{asset('plugins/fullcalendar-daygrid/main.min.js')}}"></script>
  <script src="{{asset('plugins/fullcalendar-timegrid/main.min.js')}}"></script>
  <script src="{{asset('plugins/fullcalendar-interaction/main.min.js')}}"></script>
  <script src="{{asset('plugins/fullcalendar-bootstrap/main.min.js')}}"></script>
  <script src="{{asset('plugins/sweetalert2/sweetalert2.js')}}"></script>
  <script src="{{ asset('plugins/toastr/toastr.min.js') }}?v={{ config('app.version') }}"></script>
  <!-- Page specific script -->
  <script>
$(function() {
    $.ajaxSetup({
        headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
</script>
  <script>
    function compare( a, b ) {
      var startA = moment(a.start,"DD MM YYYY H:m:s");
      var startB = moment(b.start,"DD MM YYYY H:m:s");
      if ( startA.isAfter(startB) ){
        return 1;
      }
      if ( startB.isAfter(startA) ){
        return -1;
      }
      return 0;
    }
    $(function () {
      //getting the events 
      var eventSelceted =""; 
      var events = [ 
      @foreach($events as $event)
      {
        title: "{{$event['eventTitle']}}",
        start: new Date("{{$event['start']}}"),
        end: new Date("{{$event['end']}}"),
        stick: true,
        backgroundColor: "{{$event['backgroundColor']}}", 
        borderColor    : "{{$event['borderColor']}}",
        _id: "{{$event['id']}}",
        company: "{{$event['company']}}" 
      },
      @endforeach
     ];
     events.sort(compare);
     @if(session()->has('user'))
      var i=0; 
      while (i<events.length - 1) 
      {
        endA = moment(events[i].end,"DD MM YYYY H:m:s");
        startB = moment(events[i+1].start,"DD MM YYYY H:m:s");
        if ((events[i].backgroundColor == "#f56954")&&(events[i+1].backgroundColor == "#f56954")) // verify if both events are not connected user's events
        {
          if ((endA.isAfter(startB))||(endA.isSame(startB))) 
            {
                events[i].end = events[i+1].end; 
                events.splice(i+1,1); 
            }else {
              i++; 
            }
        } else i++;
      }
     @endif
      /* initialize the external events
       -----------------------------------------------------------------*/
      function ini_events(ele) {
        ele.each(function () {
  
          // create an Event Object (https://fullcalendar.io/docs/event-object)
          // it doesn't need to have a start or end
          var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
          }
  
          // store the Event Object in the DOM element so we can get to it later
          $(this).data('eventObject', eventObject)
  
          // make the event draggable using jQuery UI
          $(this).draggable({
            zIndex        : 1070,
            revert        : true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
          })
  
        })
      }
  
      ini_events($('#external-events div.external-event'))
  
      /* initialize the calendar
       -----------------------------------------------------------------*/
      //Date for the calendar events (dummy data)
      var date = new Date()
      var d    = date.getDate(),
          m    = date.getMonth(),
          y    = date.getFullYear()
  
      var Calendar = FullCalendar.Calendar;
      var calendarEl = document.getElementById('calendar');
      var calendar = new Calendar(calendarEl, {
        locale: 'fr',
        plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
        defaultView: 'timeGridWeek',
        maxTime : '17:00',
        minTime : '8:00',
        aspectRatio: 1.673,
        allDaySlot: false,
        nowIndicator: true,
        header    : {
          left  : 'prev,next today',
          center: 'title',
          right : 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        'themeSystem': 'bootstrap',
        //businessHours : 
        businessHours :[
          {
          daysOfWeek: [1,2,3,4],
          startTime: '8:00', 
          endTime: '17:00',
          },
          {
          daysOfWeek: [0],
          startTime: '8:00', 
          endTime: '17:00',
          },
        ],
        selectConstraint: "businessHours",
        //Random default events
        events    : events,
        selectable : true,
        @if(session()->has('user'))
        select : function (selectionInfo ) {

          if (selectionInfo.allDay) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'vous devez choisir une peroide dans la plage horraire d\'un jour !'
              }); 
          }
          else {
            var eventTitle = ""; 
            moment.locale('fr');
            var start = moment(selectionInfo.start,"DD MM YYYY H:m:s");
            var end   = moment(selectionInfo.end,"DD MM YYYY H:m:s");
            if (start.isAfter(moment())) 
            {  
              Swal.fire({
                titleText: 'Réserver Rendu-vous',
                html : '<div class="d-flex flex-row justify-content-start"><p><br>  Créneau horraire : <b> '+ start.format("YYYY-MM-DD hh:mm") +'>>'+end.format("hh:mm") + '</b><br> <br> Donnez un titre à ce Rendu-Vous : &nbsp&nbsp&nbsp&nbsp </p></div>' ,
                input: 'text',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Réserver',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !Swal.isLoading()
              }).then((result) => {
                  eventTitle = result.value;
                  url = "{{route('propose')}}";
                  $.ajax({
                          method: 'POST',
                          url: url,
                          data: {
                                  "label"  : eventTitle,
                                  "start"  : start.format("YYYY MM DD H:mm:ss"),
                                  "end"    : end.format("YYYY MM DD H:mm:ss"),
                          },
                          type: "post",
                          success: function(data) {
                            calendar.addEvent(
                                {
                                  title: eventTitle,
                                  start: selectionInfo.start,
                                  end: selectionInfo.end,
                                  stick: true,
                                  backgroundColor: '#3085d6', //blue
                                  borderColor    : '#3085d6' //blue
                                }); 
                                moment.locale('fr');
                                toastr.info('votre rendu vous est réservé pour: <br>' + start.format("MMMM-DD hh:mm"));
                            },
                          error: function(data) {
                            toastr.error(data.responseJSON.message); 
                            }
                  });
              });
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Vous ne pouvez pas choisir une date passée !',
              }); 
            }
          }
        },
        @endif 
        eventClick:  function(el, jsEvent, view) {
          moment.locale('fr');
          @if(session()->has('user'))
          company = "{{session('user')['company']}}";
          if (company == el.event.extendedProps.company) {
          endtime = moment(el.event.end).format('HH:mm');
          starttime = moment(el.event.start).format('dddd, Do MMMM YYYY, HH:mm');
          var mywhen = starttime + ' - ' + endtime;
          $('#modalTitle').html(el.event.title+' - '+el.event.extendedProps.company);
          $('#modalWhen').text(mywhen);
          $('#eventID').val(el.event.extendedProps._id);
          eventSelceted = el.event; 
          $('#calendarModal').modal();
          }
          @endif
          @if (session()->has('admin'))
          endtime = moment(el.event.end).format('HH:mm');
          starttime = moment(el.event.start).format('dddd, Do MMMM YYYY, HH:mm');
          var mywhen = starttime + ' - ' + endtime;
          $('#modalTitle').html(el.event.title+' - '+el.event.extendedProps.company);
          $('#modalWhen').text(mywhen);
          $('#eventID').val(el.event.extendedProps._id);
          eventSelceted = el.event; 
          $('#calendarModal').modal();
          @endif 
        }
      });
  
      calendar.render();
      // $('#calendar').fullCalendar()
      @if(session()->has('admin'))
          $('#proveButton').on('click', function(e){
              // We don't want this to act as a link so cancel the link action
              e.preventDefault();
              Swal.fire({
                title: 'étes-Vous sûre?',
                text: "cette rendu-vous sera marquée comme faite avec présence du client!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer!',
                cancelButtonText: 'annuler!'
              }).then((result) => {
                if (result.value) {
                  $("#calendarModal").modal('hide');
                  var eventID = $('#eventID').val();
                $.ajax({
                  method: 'POST',
                  url: "{{route('present')}}",
                  data: {
                    "_id" : eventID,
                  },
                  type: "POST",
                  success: function(data) {
                      event = eventSelceted;
                      var newEvent = {
                                  title: event.title,
                                  start: event.start,
                                  end: event.end,
                                  stick: true,
                                  backgroundColor: "#00a65a", //Success (green)
                                  borderColor    : "#00a65a" //Success (green)
                        }
                      event.remove();
                      calendar.addEvent(newEvent);   
                  Swal.fire(
                    'succés!',
                    'opération terminée avec succés',
                    'success'
                  )
                  },
                  error: function(data) {
                     toastr.error(data.responseJSON.message); 
                    }
                });
                }
              })
          });
          
          $('#deleteButton').on('click', function(e){
              // We don't want this to act as a link so cancel the link action
              e.preventDefault();
              Swal.fire({
                title: 'étes-Vous sûre?',
                text: "cette rendu-vous sera supprimé!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer!',
                cancelButtonText: 'annuler!'
              }).then((result) => {
                if (result.value) {
                  $("#calendarModal").modal('hide');
                  var eventID = $('#eventID').val();
                $.ajax({
                  method: 'POST',
                  url: "{{route('deleteRDV')}}",
                  data: {
                    "_id" : eventID,
                  },
                  type: "POST",
                  success: function(data) {
                      event = eventSelceted;
                      event.remove(); 
                  Swal.fire(
                    'succés!',
                    'opération terminée avec succés',
                    'success'
                  )
                  },
                  error: function(data) {
                     toastr.error(data.responseJSON.message); 
                    }
                });
                }
              })
          });
      @else
      $('#annulerbutton').on('click', function(e){
              // We don't want this to act as a link so cancel the link action
              e.preventDefault();
              Swal.fire({
                title: 'étes-Vous sûre?',
                text: "ce rendu-vous sera annulé!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmer!',
                cancelButtonText: 'annuler!'
              }).then((result) => {
                if (result.value) {
                  $("#calendarModal").modal('hide');
                  var eventID = $('#eventID').val();
                $.ajax({
                  method: 'POST',
                  url: "{{route('annulerRDV')}}",
                  data: {
                    "_id" : eventID,
                  },
                  type: "POST",
                  success: function(data) {
                      event = eventSelceted;
                      event.remove();
                      toastr.info(data.message);
                  },
                  error: function(data) {
                    console.log(data);
                     toastr.error(data.responseJSON.message); 
                    }
                });
                }
              })
          });
      @endif
        }); 
  </script>
@endsection