<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
          <i class="mdi mdi-home"></i>
        </span> {{$title ?? ''}}
    </h3>

    @if(isset($back_title))
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                    <h5 class="pg-title staff-title"><a href="{{ url($back_url) }}">{{ $back_title }}</a></h5>
                </li>
            </ul>
        </nav>
    @endif
</div>

