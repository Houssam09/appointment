$(document).ready( function () {

    /**
     * general ajax csrf token
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * delete trigger & function "swal"
     */

    $(document).on('click', '.delete', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var success_msg = $(this).data('success');
        var url = $(this).data('url');
        deleteData(id, url, success_msg);
    });


    function deleteData(id, url, success_msg){
        Swal.fire({
            title: 'هل أنت متأكد؟',
            text: "لن تستطيع إسترجاع المعلومات المحذوفة!",
            type: "warning",
            icon: "warning",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "نعم!",
            cancelButtonText: 'لا، إلغاء!',
            showCancelButton: true,
        })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        url : url,
                        type : "POST",
                        data : {'_method' : 'DELETE', 'id': id},
                        success: function(data){
                            Swal.fire({
                                type: "sucsess",
                                text: success_msg,
                                icon: "success",
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function() {
                                location.reload();
                            });
                        },
                        error : function(){
                            Swal.fire({
                                title: 'حدث خطأ...',
                                text : 'خطأ',
                                type : 'error',
                                timer : 1500
                            }).then(function() {
                                location.reload();
                            });
                        }
                    })
                } else {
                    //location.reload();
                }
            });
    }

    /**
     * approve trigger & function "swal"
     */

    $(document).on('click', '.approve', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var success_msg = $(this).data('success');
        var url = $(this).data('url');
        approveData(id, url, success_msg);
    });


    function approveData(id, url, success_msg){

        Swal.fire({
            title: 'هل أنت متأكد؟',
            text: "سيتم تغيير حالة المهمة إلى 'مؤكدة'!",
            type: "warning",
            icon: "warning",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "نعم!",
            cancelButtonText: 'لا، إلغاء!',
            showCancelButton: true,
        })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        url : url,
                        type : "POST",
                        data : {'_method' : 'PATCH', 'id': id},
                        success: function(data){
                            Swal.fire({
                                type: "sucsess",
                                text: success_msg,
                                icon: "success",
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function() {
                                location.reload();
                            });
                        },
                        error : function(){
                            Swal.fire({
                                title: 'حدث خطأ...',
                                text : 'خطأ',
                                type : 'error',
                                timer : 1500
                            }).then(function() {
                                location.reload();
                            });
                        }
                    })
                } else {
                    //location.reload();
                }
            });
    }

    /**
     * initiate datatable "all app tables"
     */
    $('#myTable').DataTable({
        responsive: true,
        "lengthMenu": [ 50 ],
        "language": {
            "url": "../js/DataTables/i18n/Arabic.json"
        },
    });

    /**
     * initiate datepicker for filtering "month page"
     */

    $('.datepicker').datepicker({
        format: 'yyyy/mm/dd',
        language: 'fr-FR',
        date: new Date()
    });

    /**
     * initiate select2 (clients page)
     */

    $('.select-js').select2({
        placeholder: 'إختر طالب من القائمة',
        closeOnSelect: false
    });





    var timepicker_init_val = $('.timepicker').attr('value');

    var timepicker_params = {
        timeFormat: 'HH:mm',
        interval: 15,
        minTime: '08',
        maxTime: '22',
        startTime: '08',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    };
    if(timepicker_init_val == ''){
        timepicker_params.defaultTime = '08';
    }else{
        timepicker_params.defaultTime = timepicker_init_val;
    }

    $('.timepicker').timepicker(timepicker_params);


    $(document).on('click', '#addRow', function () {
        var $this = $(this);
        var num = parseInt($('.rows-num').val()) + 1;
        $.ajax({
            type:"POST",
            url: "/rdv-platform/public/admin/appointment/getMonthsDays",
            success: function(data){
                var months = data.months;
                var days = data.days;
                var row = '<div class="row custom-row">' +
                    '<div class="col-2">' +
                    '<select name="custom_months_'+ num +'[]" class="form-control select-js" multiple>';
                    $.each(months, function (i,v) {
                        row += '<option value="'+ i +'">'+ v +'</option>';
                    });


                    row += '</select>' +
                    '</div>' +
                    '<div class="col-2">' +
                    '<select name="custom_days_'+ num +'[]" class="form-control select-js" multiple>' ;
                        $.each(days, function (i,v) {
                            row += '<option value="'+ i +'">'+ v +'</option>';
                        });
                    row += '</select>' +
                    '</div>' +
                    '<div class="col-5">' +
                    '<div class="row">' +
                    '<div class="col-6">\n' +
                    '<input class="form-control " placeholder="Heure de début" name="custom_work_start_'+ num +'" type="text" id="work_start">' +
                    '<span class="invalid-feedback"></span>\n' +
                    '</div>' +
                    '<div class="col-6">' +
                    '<input class="form-control " placeholder="Heure de fin" name="custom_work_end_'+ num +'" type="text" id="work_end">' +
                    '<span class="invalid-feedback"></span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                     '<div class="col-2">\n' +
                    '<input type="text" name="custom_hvolume_'+ num +'" class="form-control" placeholder="volume horaire">' +
                    '<span class="invalid-feedback"></span>' +
                    '</div>'+
                    '<div class="col-1">' +
                    '<button id="addRow" type="button" class="btn btn-sm btn-success">+</button>' +
                    '</div>' +
                    '</div>';

                $this.parents('.custom-row').after(row);
                var button = '<button id="removeRow" type="button" class="btn btn-sm btn-danger">-</button>';
                $this.parents('.custom-row').children('.col-1').append(button);
                $this.parents('.custom-row').find('#addRow').remove();
                //console.log(button)
                $('.select-js').select2({
                    placeholder: 'إختر طالب من القائمة',
                    closeOnSelect: false
                });
            }
        })

        $('.rows-num').val(parseInt($('.rows-num').val()) +1);


    })

    $(document).on('click', '#removeRow', function () {
        $(this).parents('.custom-row').remove();
        $('.rows-num').val(parseInt($('.rows-num').val()) -1);
    })

} );
