/* var dd = new Date();
 var volume = $('#calendar').data('volume');
 var year = dd.getFullYear();
 var month = dd.getMonth() + 1;
 month = ("0" + month).slice(-2);
 var numDays = new Date(dd.getFullYear(), dd.getMonth() + 1, 0).getDate();


   document.addEventListener('DOMContentLoaded', function() {

     var calendarEl = document.getElementById('calendar');

     var calendar = new FullCalendar.Calendar(calendarEl, {
       plugins: [ 'dayGrid', 'timeGrid', 'bootstrap', 'interaction',  ],
       contentHeight: 'auto',
       defaultView: 'timeGridTenDays',
       themeSystem: 'bootstrap',
       timeZone: 'local',
       header: {
         left: '',
         center: '',
         right: 'timeGridDay'
       },
       //buttonText:{
       //  timeGridTenDays: "10 Jours"
       //},
     navLinks: false,
       locale: 'fr',
       views: {
         timeGridTenDays: {
           type: 'timeGrid',
           slotLabelFormat: {
               hour12: false,
               hour: '2-digit',
               minute: '2-digit'
             },

           minTime: "08:00:00",
           maxTime: "18:00:00",
           hiddenDays: [ 5,6 ],
           visibleRange: {
             start: year + '-' + month + '-20',
             end: year + '-' + month + '-' + numDays
           }
         },

       },

       events: 'controllers/feed.php?view=1',


     });

     calendar.render();
   });

   */


var dd = new Date();
var volume = $('#calendar').data('volume');
var year = dd.getFullYear();
var month = dd.getMonth() + 1;
month = ("0" + month).slice(-2);
//var numDays = new Date(dd.getFullYear(), dd.getMonth() + 1, 0).getDate();
var startDay = $('#calendar').data('first');
var lastDay = $('#calendar').data('last');


document.addEventListener('DOMContentLoaded', function() {

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid', 'timeGrid', 'bootstrap', 'interaction',  ],
        contentHeight: 'auto',
        defaultView: 'dayGridMonth',  /*'timeGridTenDays',*/
        navLinks: true,
        themeSystem: 'bootstrap',
        selectable: true,
        timeZone: 'local',
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'dayGridMonth timeGridDay'
        },
        /*minTime: "07:00:00",
        maxTime: "14:00:00",*/
        hiddenDays: [ 5,6 ],
        /*buttonText:{
          timeGridTenDays: "10 Jours"
        },*/
        //navLinks: false,
        locale: 'fr',
        firstDay: 0,
        views: {

            timeGridTenDays: {
                type: 'timeGrid',
                slotLabelFormat: {
                    hour12: false,
                    hour: '2-digit',
                    minute: '2-digit'
                },

                minTime: "07:00:00",
                maxTime: "15:00:00",
                hiddenDays: [ 5,6 ],
                visibleRange: {
                    start: /*year + '-' + month + '-20',*/ startDay,
                    end: /*year + '-' + month + '-' + numDays*/ moment(lastDay).add(1, 'days').format('YYYY-MM-DD')
                }
            },

        },

        events: 'controllers/feed.php?view=1',
        eventClick:  function(el, jsEvent, view) {

            endtime = moment(el.event.end).format('HH:mm');
            starttime = moment(el.event.start).format('dddd, Do MMMM YYYY, HH:mm');
            var mywhen = starttime + ' - ' + endtime;
            $('#modalTitle').html(el.event.title);
            $('#modalWhen').text(mywhen);
            $('#eventID').val(el.event.id);
            $('#calendarModal').modal();

        }

        /*select: function(start, startStr, end, jsEvent) {

            starttime = moment(start.startStr).format('dddd, Do MMMM YYYY, HH:mm');
            endtime = moment(start.startStr).add(volume, 'hours').format('HH:mm');
            mywhen = starttime + ' - ' + endtime;
            var t = start.startStr;
            start = moment(t).format('YYYY-MM-DD HH:mm');
            end = moment(t).add(volume, 'hours').format('YYYY-MM-DD HH:mm');
            $('#createEventModal #startTime').val(start);
            $('#createEventModal #endTime').val(end);
            $('#createEventModal #when').text(mywhen);
            $('#createEventModal').modal('toggle');
         },

        eventClick:  function(el, jsEvent, view) {
          if(el.event.id == '1'){
            endtime = moment(el.event.end).format('HH:mm');
            starttime = moment(el.event.start).format('dddd, Do MMMM YYYY, HH:mm');
            var mywhen = starttime + ' - ' + endtime;
            $('#modalTitle').html(el.event.title);
            $('#modalWhen').text(mywhen);
            $('#eventID').val(el.event.id);
            $('#calendarModal').modal();
            }
          },*/

    });

    calendar.render();

    $('#deleteButton').on('click', function(e){
        // We don't want this to act as a link so cancel the link action
        e.preventDefault();
        doDelete();
    });

    function doDelete(){
        $("#calendarModal").modal('hide');
        var eventID = $('#eventID').val();
        $.ajax({
            url: 'controllers/feed.php',
            data: 'action=delete&id='+eventID,
            type: "POST",
            success: function(json) {
                if(json == 1)
                    calendar.refetchEvents();
                else
                    return false;


            }
        });
    }

    $('#proveButton').on('click', function(e){
        // We don't want this to act as a link so cancel the link action
        e.preventDefault();
        doProve();
    });

    function doProve(){
        $("#calendarModal").modal('hide');
        var eventID = $('#eventID').val();
        $.ajax({
            url: 'controllers/feed.php',
            data: 'action=prove&id='+eventID,
            type: "POST",
            success: function(json) {
                if(json == 1)
                    calendar.refetchEvents();
                else
                    return false;


            }
        });
    }




});
